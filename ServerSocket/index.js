var express = require ('express');
const fetch = require('node-fetch');
const socketIo = require('socket.io');
var app = express();
var port = 9000;

const server = app.listen(port);
const io = require('socket.io')(server);

io.on('connection', (socket) =>{		
   fetch('http://localhost:1337/api/feeds')

   .then(resp => resp.json())

   .then((json) => {
      socket.emit('feeds', json);
   })
});

console.log(`Server listening in on port ${port}`);